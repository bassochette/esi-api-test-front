import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as _ from 'lodash';
import { Button, Col, Row } from 'reactstrap';
import { v4 as uuid } from 'uuid';

import '../styles/Characters.scss';

class GridLayout extends Component {
    constructor(props) {
        super(props);

        this.state = {
            character: null,
            components: [],
        };
    }

    componentDidMount() {
        this.addPanel();
    }

    renderComponent = (component) => {
        const { dependencies, character } = this.props;
        return(
            <Col xs={component.width || 12} key={component.id}>
                <component.type
                    dependencies={dependencies}
                    character={character}
                    close={() => this.setState({ components: _.filter(this.state.components, c => c.id !== component.id)})}
                />
            </Col>
        );
    };

    addPanel = () => {
        const id = uuid();
        const { panelType, width = 12 } = this.props;
        this.setState({
            components: [...this.state.components, {
                type: panelType,
                id,
                width,
            }],
        });
    };

    render() {
        return (
            <div className='content'>
                <Row>
                    {_.map(this.state.components, this.renderComponent)}
                </Row>
                <Row>
                    <Col xs={12}>
                        <Button color='secondary' onClick={() => this.addPanel()}>Add panel</Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

GridLayout.propTypes = {
    dependencies: PropTypes.object,
    character: PropTypes.number,
    panelType: PropTypes.any,
};

export default GridLayout;
