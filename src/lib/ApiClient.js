import * as axios from 'axios';

export class ApiClient {
    constructor(config) {
        this.config = config;
    }

    // Characters

    getCharacters() {
        return this.get(`/characters/`);
    }

    newCharacter() {
        return this.post(`/characters/`);
    }

    refreshCharacter(characterId) {
        return this.post(`/characters/${characterId}/refresh`);
    }

    deleteCharacter(characterId) {
        return this.delete(`/characters/${characterId}`);
    }

    // Trade watcher

    startTradeWatcher(characterId, stationId) {
        return this.post(`/characters/${characterId}/trades-watcher/${stationId}`);
    }

    bumpTradeWatcher(characterId, stationId) {
        return this.post(`/characters/${characterId}/trades-watcher/${stationId}/heartbeat`);
    }

    // Trade opportunities

    getAllOpportunities() {
        return this.get(`/trade-opportunities`);
    }

    computeTradeOpportunities(srcRegionId, dstRegionId, options, state = '') {
        return this.post(`/trade-opportunities?state=${state}`, { data: { srcRegionId, dstRegionId, options } });
    }

    deleteTradeOpportunity(opportunityId) {
        return this.delete(`/trade-opportunities/${opportunityId}`);
    }

    // Trade reports

    getAllTradeReports(characterId) {
        return this.get(`/characters/${characterId}/trade-reports`);
    }

    createTradeReport(character, srcRegionId, dstRegionId, state = '') {
        return this.post(`/characters/${character}/trade-reports?state=${state}`, { data: { srcRegionId, dstRegionId } });
    }

    deleteTradeReport(character, reportId) {
        return this.delete(`/characters/${character}/trade-reports/${reportId}`);
    }

    // Query methods

    get(path, options = {}) {
        const { apiUrl } = this.config;
        return axios({
            method: 'GET',
            url: `${apiUrl}${path}`,
            ...options,
        })
            .then(resp => resp.data)
            .catch(this.errorHandler);
    }

    post(path, options = {}) {
        const { apiUrl } = this.config;
        return axios({
            method: 'POST',
            url: `${apiUrl}${path}`,
            ...options,
        }).catch(this.errorHandler);
    }

    delete(path, options = {}) {
        const { apiUrl } = this.config;
        return axios({
            method: 'DELETE',
            url: `${apiUrl}${path}`,
            ...options,
        }).catch(this.errorHandler);
    }

    errorHandler(err) {
        throw err.response;
    }
}
