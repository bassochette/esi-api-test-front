import * as axios from 'axios';

export class DataStore {
    constructor(config, apiClient) {
        this.config = config;
        this.apiClient = apiClient;
        this.cache = {};
    }

    initialize() {
        return Promise.all([
            this.fetchAndCache('/regions'),
        ]);
    }

    fetchAndCache(url) {
        return axios.get(`${this.config.apiUrl}${url}`)
            .then(response => {
                this.cache[url] = response.data;
                return response.data;
            });
    }

    cachedFetch(url) {
        return this.cache[url] ? Promise.resolve(this.cache[url]) : this.fetchAndCache(url);
    }
}
