import React from 'react';
import moment from 'moment';
import numeral from 'numeral';
import * as _ from 'lodash';
import { Button } from 'reactstrap';
import { v4 as uuid } from 'uuid';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export class Format {
    static isk(input, withUnit = false) {
        const numberIn = typeof input === 'string' ? parseInt(input) : input;
        return `${numeral(numberIn).format('0,0.00')}${withUnit ? ' ISK' : ''}`;
    }

    static iskStringToNumber(str) {
        return numeral(str.replace(/ ISK$/i, '')).value();
    }

    static dateTime(input) {
        return moment(input).locale('en-UK').format('ll');
    }

    static shortDuration(dateA, dateB) {
        const duration = moment.duration(moment(dateA).diff(dateB));
        console.log(`updateAt=${dateA} now=${dateB} duration=${duration}`);
        const pad = s => _.padStart(s, 2, '0');
        return duration.as('seconds') > 0 ? `${pad(duration.hours())}:${pad(duration.minutes())}:${pad(duration.seconds())}` : null;
    }

    // static number
}

export class ColumnUtils {
    static dateCell(row) {
        return moment(row.value).locale('en-UK').format('ll');
    }

    static dataTimeCell(row) {
        return moment(row.value).locale('en-UK').format('lll');
    }

    static numberCell(row) {
        return (
            <div style={{ textAlign: 'right' }}>
                {Format.isk(row.value)}
            </div>
        );
    }

    static actionIconsCell(icons) {
        return (
            <div>
                {_.map(icons, i => (
                    <div className='grid-icon' key={uuid()}>
                        <Button
                            className='btn-icon btn-simple'
                            color='link'
                            size='sm'
                            onClick={i.callback}
                        >
                            <FontAwesomeIcon icon={i.iconClass} size='sm' />
                        </Button>
                    </div>
                ))}
            </div>
        )
    }

    static iskCell(row) {
        return (
            <div style={{ textAlign: 'right' }}>
                {Format.isk(row.value)}<small style={{ marginLeft: 2 }}>ISK</small>
            </div>
        )
    }

    static boolCell(row) {
        return (
            <div className='center'>
                {row.value !== null && <FontAwesomeIcon icon={row.value ? 'check' : 'times'} color={row.value ? 'green' : 'red'}/>}
            </div>
        );
    }
}
