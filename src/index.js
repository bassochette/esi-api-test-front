import React from 'react';
import ReactDOM from 'react-dom';
import { createBrowserHistory } from 'history';
import { Route, Router, Switch } from 'react-router-dom';

import './styles/index.css';
import App from './App';
import { config } from './config';

import './assets/scss/black-dashboard-react.scss';
import { Dependencies } from './lib/Dependencies';

const hist = createBrowserHistory();
const dependencies = Dependencies.build(config);

ReactDOM.render(
    <Router history={hist}>
        <Switch>
            <Route path='/' render={props => <App {...props} dependencies={dependencies} />} />
        </Switch>
    </Router>,
    document.getElementById('root'));
