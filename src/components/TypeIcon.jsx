import React from 'react';
import PropTypes from 'prop-types';

export class TypeIcon extends React.Component {
    render() {
        const { typeId, inEsiGrid, dependencies: { config } } = this.props;
        return (
            <div className={`type_icon ${inEsiGrid ? 'grid-type-icon' : ''}`}>
                <img alt='type-icon' src={`${config.imageApiUrl}/Type/${typeId}_32.png`} />
            </div>
        );
    }
}

TypeIcon.propTypes = {
    dependencies: PropTypes.object,
    typeId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    inEsiGrid: PropTypes.bool,
};
