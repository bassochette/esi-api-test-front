import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as _ from 'lodash';
import Select from 'react-select';
import 'react-select'

import '../styles/RegionSelector.scss';

class StationSelector extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: null,
            data: [],
            error: null,
        }
    }

    componentDidMount() {
        const { dataStore } = this.props.dependencies;
        dataStore.cachedFetch('/stations/hubs')
            .then(data => this.setState({ data }))
            .catch(err => this.setState({ error: err }));
    }

    onChange(event) {
        this.props.onChange(event.value);
    }

    render() {
        const { className } = this.props;
        return (
            <div className={`StationSelector ${className}`}>
                {this.state.error && <div className='char-error'>{this.state.error.toString()}</div>}
                <Select
                    className='esi-select'
                    classNamePrefix='esi-select'
                    isDisabled={this.props.disabled}
                    options={_.map(this.state.data, d => ({ value: d.station_id, label: d.name }))}
                    onChange={e => this.onChange(e)}
                />
            </div>
        );
    }
}

StationSelector.propTypes = {
    onChange: PropTypes.func,
    disabled: PropTypes.bool,
    dependencies: PropTypes.object,
};

export default StationSelector;
