import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { v4 as uuid } from 'uuid';
import { Button, Card, CardBody, CardHeader, CardTitle, Col, Row } from 'reactstrap';
import ReactTable from 'react-table';
import moment from 'moment';
import * as _ from 'lodash';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import 'react-table/react-table.css';

import { ColumnUtils } from '../lib/Format';
import RegionSelector from './RegionSelector';
import ProgressBar from './ProgressBar';

import '../styles/Characters.scss';
import '../styles/EsiTable.scss';
import '../styles/TradeReport.scss';

const View = {
    LIST: 'list',
    CREATE_FORM: 'createForm',
    VIEW_FORM: 'viewForm',
};
const SseEvent = {
    TRADE_REPORT_UPDATED: 'trade-reports-updated',
    TRADE_REPORT_COMPUTED: 'trade-reports-computed',
    TRADE_REPORT_PROGRESS: 'trade-reports-progress',
    TRADE_REPORT_ERROR: 'trade-reports-error',
};

class TradeReport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentView: View.LIST,
            allReports: [],
            srcRegion: null,
            dstRegion: null,
            openedReport: {},
            requestState: null,
            error: null,
            collapsed: false,
        };

        this.props.dependencies.sseClient.on(SseEvent.TRADE_REPORT_COMPUTED, report => this.reportComputed(report));
        this.props.dependencies.sseClient.on(SseEvent.TRADE_REPORT_PROGRESS, report => this.reportProgress(report));
        this.props.dependencies.sseClient.on(SseEvent.TRADE_REPORT_ERROR, error => this.setState({ error, requestState: null }));
        this.props.dependencies.sseClient.on(SseEvent.TRADE_REPORT_UPDATED, () => this.fetchAllReports());
    }

    componentDidMount() {
        if (this.props.character) {
            this.fetchAllReports();
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.character !== this.props.character) {
            this.setState({ openedReport: {}, allReports: [], currentView: View.LIST });
            this.fetchAllReports();
        }
    }

    // ===== Life Cycle ===== //

    toggleCollapsed() {
        this.setState({ collapsed: !this.state.collapsed });
    }

    // ===== XHR calls and callbacks ===== //

    fetchAllReports() {
        const { dependencies: { apiClient }, character } = this.props;
        console.log(`Updating trade reports`);
        apiClient.getAllTradeReports(character)
            .then(allReports => this.setState({ allReports }))
            .catch(error => this.setState({ error }));
    }

    handleSubmit() {
        const { srcRegion, dstRegion } = this.state;
        const { dependencies: { apiClient }, character } = this.props;
        const requestState = uuid();
        this.setState({ requestState });

        apiClient.createTradeReport(character, srcRegion, dstRegion, requestState)
            .catch(error => this.setState({ error }));
    }

    reportProgress({ state, stages }) {
        if (state === this.state.requestState) {
            this.setState({ progress: stages });
        }
    }

    reportComputed({ state, ...data }) {
        if (state === this.state.requestState) {
            this.setState({
                allReports: [...this.state.allReports, data],
                openedReport: data,
                currentView: View.VIEW_FORM,
                requestState: null,
            });
        }
    }

    // ===== Render methods ===== //

    renderReportsList = () => {
        const { allReports } = this.state;
        const { character, dependencies: { apiClient } } = this.props;
        return (
            <CardBody className='trade-reports-list'>
                <ReactTable
                    data={allReports}
                    className='-striped -highlight esi-table'
                    columns={[
                        { id: 'date', Header: 'Date', accessor: 'date', Cell: row => moment(row.value).format('lll') },
                        { id: 'srcRegion', Header: 'From region', accessor: d => d.srcRegionName },
                        { id: 'dstRegion', Header: 'To region', accessor: d => d.dstRegionName },
                        { id: 'nbType', Header: 'Nb. types in report', maxWidth: 100, accessor: d => d.reports.length },
                        {
                            id: 'actions', Header: 'Actions', maxWidth: 50,
                            accessor: d => ColumnUtils.actionIconsCell([
                                { iconClass: 'external-link-alt', callback: () => this.setState({ currentView: View.VIEW_FORM, openedReport: d }) },
                                { iconClass: 'trash-alt', callback: () => apiClient.deleteTradeReport(character, d.id) },
                            ]),
                        },
                    ]}
                    defaultSorted={[
                        { id: 'date', desc: true },
                    ]}
                    defaultPageSize={10}
                />
                <Button disabled={!character} onClick={() => this.setState({ currentView: View.CREATE_FORM })}>
                    Create a new report
                </Button>
            </CardBody>
        );
    };

    renderCreateForm = () => {
        const { srcRegion, dstRegion, requestState, progress } = this.state;
        const { dependencies, character } = this.props;
        const isSubmitable = srcRegion && dstRegion && character && !requestState;

        return (
            <CardBody className='trade-reports-params'>
                <Row>
                    <Col xs={6}>Source region: <RegionSelector disabled={!!requestState} dependencies={dependencies} onChange={r => this.setState({ srcRegion: r })}/></Col>
                    <Col xs={6}>Destination region: <RegionSelector disabled={!!requestState} dependencies={dependencies} onChange={r => this.setState({ dstRegion: r })}/></Col>
                    <Col xs={12}>
                        <Button onClick={() => this.setState({ currentView: View.LIST })}>
                            Back to list
                        </Button>
                        <Button color='success' disabled={!isSubmitable} onClick={() => this.handleSubmit()}>
                            Build reports
                        </Button>
                    </Col>
                </Row>
                {requestState && <CardBody>
                    <div>Computing trade reports... <i className='tim-icons icon-refresh-01 loader-rotate'/></div>
                    <div>Computing trade opportunities... <i className='tim-icons icon-refresh-01 loader-rotate'/></div>
                    {_.map(progress, p => <ProgressBar center key={uuid()} progress={p}/>)}
                </CardBody>}
            </CardBody>
        );
    };

    renderReportForm = () => {
        const { openedReport } = this.state;
        const { dependencies: { config } } = this.props;
        const align = (textAlign = 'right') => row => <div style={{ textAlign }}>{row.value}</div>;
        return (
            <>
            {openedReport.id && (<div className='esi-panel-header-state'>
                    {openedReport.srcRegionName}
                    <FontAwesomeIcon icon='angle-double-right' style={{ margin: '0 8px'}} color='orange'/>
                    {openedReport.dstRegionName}
                </div>)}
            <CardBody className='trade-report-form'>
                    {/*<h2 className='center'>{openedReport.srcRegionName} <FontAwesomeIcon icon='angle-double-right'/> {openedReport.dstRegionName}</h2>*/}
                    <ReactTable
                        className='-striped -highlight report-lines-table esi-table'
                        data={openedReport.reports}
                        columns={[
                            { id: 'typeIcon', Header: '', width: 35, accessor: d => <div className='grid-type-icon'><img alt='type-icon' src={`${config.imageApiUrl}/Type/${d.type.type_id}_32.png`} /></div> },
                            { id: 'typeName', Header: 'Type', width: 320, accessor: d => d.type.name, Cell: align('left') },
                            { Header: 'Bought', width: 70, accessor: 'boughtItems', Cell: align() },
                            { Header: 'Sold', width: 70, accessor: 'soldItems', Cell: align() },
                            { Header: 'Remaining', width: 70, accessor: 'remainingItems', Cell: align() },
                            { accessor: 'averageBuyPrice', Header: 'Average buy price', width: 120, Cell: ColumnUtils.iskCell },
                            { accessor: 'averageSellPrice', Header: 'Average sell price', width: 120, Cell: ColumnUtils.iskCell },
                            { id: 'totalBuyPrice', accessor: d => d.averageBuyPrice * d.boughtItems, Header: 'Total buy price', width: 120, Cell: ColumnUtils.iskCell },
                            { id: 'totalSellPrice', accessor: d => d.averageSellPrice * d.soldItems, Header: 'Total sell price', width: 120, Cell: ColumnUtils.iskCell },
                            { id: 'roi', Header: 'ROI', width: 40, Cell: ColumnUtils.numberCell,
                                accessor: d => (d.averageSellPrice * d.soldItems) / (d.averageBuyPrice * d.boughtItems) },
                            { accessor: 'profit', Header: 'Profit', width: 140, Cell: ColumnUtils.iskCell },
                            { accessor: 'profitPerItem', Header: 'Profit/u', width: 140, Cell: ColumnUtils.iskCell },
                            { id: 'nbTransactions', accessor: d => d.processedTransactions.length, Header: 'Nb of transactions', Cell: align(), width: 100 },
                            { accessor: 'campaignStart', Header: 'Start', Cell: ColumnUtils.dateCell,  },
                            { accessor: 'campaignEnd', Header: 'End', Cell: ColumnUtils.dateCell },
                        ]}
                        defaultPageSize={10}
                        defaultSorted={[ { id: 'profit', desc: true } ]}
                    />
                    <div style={{ textAlign: 'left' }}>
                        <Button onClick={() => this.setState({ currentView: View.LIST, openedReport: {} })}>
                            <FontAwesomeIcon icon='arrow-alt-circle-left'/> Back to report list
                        </Button>
                    </div>
                </CardBody>
            </>
        );
    };

    render() {
        const { error, currentView, collapsed, openedReport } = this.state;

        return (
            <Card className='TradeReport esi-panel'>
                <CardHeader className='esi-panel-header'>
                    <CardTitle tag='h2'>
                        Trade Reporter
                    </CardTitle>
                    <div className='esi-panel-header-buttons'>
                        <Button size='sm' color='link' onClick={() => this.toggleCollapsed()}>
                            <FontAwesomeIcon icon={collapsed ? 'expand' : 'minus'} color='orange' />
                        </Button>
                        <Button size='sm' color='link' onClick={() => this.props.close()}>
                            <FontAwesomeIcon icon='times' color='orange' />
                        </Button>
                    </div>
                </CardHeader>

                {error && <CardBody className='error'>
                    <div className='char-error'>{JSON.stringify(error)}</div>
                </CardBody>}

                {!collapsed && currentView === View.LIST && this.renderReportsList()}
                {!collapsed && currentView === View.CREATE_FORM && this.renderCreateForm()}
                {!collapsed && currentView === View.VIEW_FORM && this.renderReportForm()}
            </Card>
        );
    }
}

TradeReport.propTypes = {
    dependencies: PropTypes.object,
    character: PropTypes.number,
    close: PropTypes.func,
};

export default TradeReport;
