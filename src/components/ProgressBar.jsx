import React from 'react';
import * as PropTypes from 'prop-types';
import { Progress } from 'reactstrap';

import '../styles/ProgressBar.scss';

class ProgressBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const { progress: { label, current, total } } = this.props;
        return (
            <div className={`esi-progress ${this.props.center && 'center'}`}>
                <div className='esi-progress-label'>{label}</div>
                <Progress className='esi-progress-bar' color='info' animated striped value={current} max={total}/>
                <div className='esi-progress-aligner' />
            </div>
        )
    }
}

ProgressBar.propTypes = {
    progress: PropTypes.shape({
        label: PropTypes.string,
        current: PropTypes.number,
        total: PropTypes.number,
    }),
    center: PropTypes.bool,
};

export default ProgressBar;
